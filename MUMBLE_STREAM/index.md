---
title: Configurar Liquidsoap para emitir sala de Mumble en Sindominio

---

# Requisitos

 * Servidor o ordenador personal con entorno gráfico disponible
 * Audio del servidor o ordenador personal bajo Pulseudio
 * Las instrucciones son para usar bajo una GNU/Linux Debian 10 con los repositorio Multimedia activados

# Paqueteria

 $ sudo apt install liquidsoap mumble pavucontrol tmux

# Entorno gráfico

Arrancamos nuestra máquina y accedemos a una sesión gráfica con una usuaria sin más privilegios que el uso del audio y la red.

Arrancamos mumble

 $ mumble

Configuramos el sonido para que funcione bajo el "default" de Pulseaudio y accedemos al Mumble de Sindominio

Datos: 

 * Servidor: charlar.sindominio.net
 * Puerto: <default>
 * User: shoutcast

Accedemos a la sala que queremos streamear y dejamos el usuario allí con el Mumble encendido.

# En la terminal

Abrimos una terminal

 $ xterm

Abrimos una sesión de tmux

 $ tmux

Y creamos un fichero que llamaremos _radio.liq_ con los siguientes contenidos

```

## más en en https://www.liquidsoap.info/
## esto conecta la entrada por defecto del ordenador y la streamea
## El bitrate esta en 192k pero se puede bajar a 128 si vemos que no tira con la conexion
## Con buena conexion estaria bonito emitir a 320 a 256.

## Requisitos
## sudo apt install liquidsoap pavucontrol


set("log.file.path","/tmp/radio.log") 
files=in()
output.icecast(%mp3(bitrate=192), files,mount="ejemplo",host="stream.piperrak.cc",port=8001,user="source",password="pedir_a_las_admins")

```

Guardamos y ejecutamos el script:

 $ liquidsoap -v radio.liq


Si todo va bien, vereis una linea que pone que la conexión ha sido correcta "successfull"

Si queremos dejar en background el proceso en nuestra _tmux_, por defecto, pulsamos a la vez _Ctrl+b_ y luego la letra _d_ para salir sin parar la terminal

Para acceder otra vez, _tmux a_ o a través de la sesiones disponibles usando _tmux ls_ y sabiendo el valor de la sesión, _tmux a -t <id>_

Dejo en link el tutorial que hicimos hace unos meses.

# Configuración del audio

Abrimos _pavucontrol_, el gestor de entradas y salidas de Pulseaudio

 $ pavucontrol

Y vamos a la pestaña "Grabación".

Allí encontraremos una entrada llamada "liquidsoap : pulse_in()" que por defecto se debería asignar a la entrada "Monitor Internal Audio", aunque depende de vuestra configuración de pulse y como se llamen las interfaces. 

El objetivo, hacer que liquidsoap streamee el sonido de la interfaz "Monitor" donde está funcionando también Mumble, es decir, se le llamada Monitor a la interfaz que reproduce lo que está sonando en ese momento en la computadora. Es decir, que si además del Mumble tenéis otras cosas sonando, como por ejemplo, un video en Peertube o una canción en vuestra instancia de Funkwhale, pues eso también saldrá por el streaming.  

Todo esto se podría reconfigurar a través de interfaces virtuales, pero no vamos a complicar la historia. Si algun_ está interesad_ en el asunto, hacemos otro tutorial.

# Testear que todo funciona

Si todo va bien, vamos a la dirección de nuestro streaming.

En este caso, usando piperrak.cc y el mount point "ejemplo" la dirección 

 http://stream.piperrak.cc:8001/ejemplo

Debería reproducir lo que está sonando en la sala donde tenéis a vuestra usuaria "shoutcast"

Si no funciona:

 * Revisar que teneis a la usuaria _shoutcast_ en la sala oportuna
 * Revisar la configuración de audio de Mumble
 * Si la usuaria no está, es que el cliente de mumble de _shoutcast_ está caído
 * Si no hay stream en la web del servidor de streaming, a lo mejor teneís mal configurado el script, revisar los mensajes de error que ofrece al arrancar _liquidsoap -v_

# Si no puedo acceder al entorno gráfico de una manera común

En este caso, configurar un servidor VNC, por ejemplo, **tigervnc** y lo arrancais en una usuaria que tenga privilegios de audio y red, y si tenéis bien configuradas las Xs en el server, podéis acceder en remoto a Mumble y arrancarlo con el usuario y pulseaudio.

Si no, podéis dejar una maquina que no estéis usando para la charla/streaming haciendo este trabajo, sin complicaros con configurar todo esto en un servidor sin Xs. Incluso, con una Tails o alguna Live con Debian debería ser posible ocnfigurar todo esto sin mucha complicación.

# Links

 * https://www.mumble.info/
 * https://wiki.archlinux.org/index.php/PulseAudio/Examples
 * https://www.liquidsoap.info/doc-dev/reference.html
 * https://www.linuxfordevices.com/tutorials/debian/install-vnc-server-on-debian
 * https://git.sindominio.net/siroco/workshop-sysadmin/src/branch/master/TMUX/index.md
