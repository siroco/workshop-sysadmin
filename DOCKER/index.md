---
title: Docker 
---

# Qué es Docker

 https://docs.docker.com/engine/

Docker es un sistema open source para la gestión de contenedores, es decir, encapsula aplicaciones con las necesidades básicas en un proceso y las aisla del resto del sistema.

Es un sistema mucho menos pesado que usar KVM o LXC para construir espacios aislados para las aplicaciones y a la vez, los contenedores son reutilizables, lo que quiere decir, que si tienes que instalar muchas veces lo mismo, te ahorras mucho tiempo y espacio.

Por ejemplo, si quieres instalar un Wordpress, deberías primero instalarte un sistema operativo, por ejemplo, una Debian, luego instalar una base de datos, por ejemplo MariaDB, luego instalar un servidor web, por ejemplo NGINX y finalmente, instalar Wordpress. Como mínimo una instalación de un Wordpress con todo el sistema puedo llegar a ocuparte ~1GB, pero, en el fondo, Wordpress no requiere todo el sistema de Debian para funcionar.

Además, si quisieras aprovechar esa Debian para otros servicios, cualquier fallo de seguirdad afectaria a todo el sistema y no solo al Wordpress atacado.

Por otro lado, es una manera muy sencilla para aprender a configurar servicios en servidores compartidos o desarrollar aplicaciones. Puedes tener diversas versiones de lo que haces en tu ordenador local y publicarlas luego en el servidor sabiendo que todo funcionará igual que en tu máquina local.

Una de las críticas a Docker sobre seguridad es que sus plantillas están gestionadas por la comunidad de Docker ( https://hub.docker.com/ ) y debes confiar que el contenido ahí publicado no tiene código malicioso y que la comunidad que mantiene esa imágen está pendiente de ella. La solución en entornos donde la seguridad es un factor importante, es posible construir tus propias imagenes de Docker en tu propio servidor, llamado en Registry en el mundo de Docker, y así evitar ese problema.

# Ejercicio práctico

Vamos a instalar Docker en un servidor junto con la herramienta llamada Docker Compose y como ejercicio, vamos a configurar un Wordpress usando estas herramientas.


Puedes instalarte tu propio Docker en tu ordenador, es lo más recomendable para aprender a usarlo.

# Pasos previos

## Install Docker-ce

La instalación de Docker está muy documentada en su página oficial.

 https://docs.docker.com/engine/install/debian/

O la gente de Digital Ocean tienen también un manual bastante sencillo de entender:

 https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-debian-10

Sigue los pasos para tu distribución. En el caso de Debian hay que desintalar cualquier aplicación vinculada a Docker del repositorio de Debian y luego configurar el repositorio oficial. Está todo documentado en la web. 

## Uso de Docker

Si has llegado a instalarlo correctamente, deberías poder hacer este comando:

```
 	$ docker -v

```

La lista de comando los puedes obtener aquí https://docs.docker.com/engine/reference/commandline/docker/

Si has usado Virsh o KVM, verás que los comandos son bastantes similares. 

```
	 $ docker ps # Muestra los contenedores activos
	 $ docker images --all # Muestra todas las imagenes descargadas
	 $ docker volume ls # Muestra los volumenes, o espacios de datos que usan los contenedores
	 $ docker network ls # Muestra las redes vinculadas a los contenedores
	 $ docker search <container> # Busca en el respositorio Hub de Docker oficial imagenes disponibles para la descarga
	 $ docker run <container> # Este comando es más largo y lo explico despues, pero con run arrancamos el container
	 $ docker rmi <nombre_imagen> # Borra la imagen descargada
	 $ docker system prune # Borra los volumenes, imagenes, configuración de red no utilizadas
```


## Buscando imagenes docker

Si hacemos una búsqueda en las docker images disponibles en [Hub(https://hub.docker.com) veremos que hay imagenes oficiales de muchos servicios, poer ejemplo Wordpress o MariaDB y MySQL

```
 	$ docker search wordpress
 	$ docker search mysql
```

## comandos útiles de docker

### arrancar un docker

Ejecutando un `docker run` descargaremos la imagen y la arrancamos, podemo a su vez añadirle parámetros a este arranque, por ejemplo el caso de MySQL.

Es la manera más rápida de arrancar una imágen de docker.

```
 	$ docker run --name db -e MYSQL_ROOT_PASSWORD=UnPasswordCualquiera -d mysql
```

Las variables disponibles para cada imagen, la puedes encontrar en la documentación en HUB [https://hub.docker.com/]

### listar contenedores activos

Para listar los contendores activos

```
 	$ docker ps 
```

### revisar logs

Para revisar los logs, puedes usar el comando

```
 	$ docker logs -f <ID_Container>
```

### parar containers

Y encontrar si alguna cosa no funciona. 

Para parar los container:

```
	$ docker stop <ID_CONTAINER>
```

### listado de todos los contenedores

Y para revisar que está parado:

```
	$ docker ps --all
```
### borrado de contenedores

Si lo queremos borrar:

```
	$ docker rm <ID_CONTAINER>
```


# Docker Compose

Esto lo que hace es "componer" un docker a nuestro gusto, sin tener que ejecutar a mano uno por uno los comandos explicados antes.


## Configurar un fichero para Docker Compose

https://docs.docker.com/compose/install/

Creamos un directrio en nuestra home y creamos el siguiente fichero **docker-compose.yml**:

Este ejemplo muestra el usado para Wordpress.
```
version: '3'
services:
  wp:
    image: wordpress:php7.1-apache
    ports:
      - "8000:80"
    environment:
      WORDPRESS_DB_PASSWORD: secret_passw
    links:
      - "mysql:db"
    volumes:
      - ./html:/var/www/html/
  mysql:
    image: "mysql:5.7"
    environment:
      MYSQL_ROOT_PASSWORD: secret_passw
    volumes:
      - ./database:/var/lib/mysql
```

Y lo arrancamos de la siguiente manera:

```
 $ docker-compose up -d
```

# Anexo: Como vincular ese Wordpress a un servicio .ONION para acceder a través de Tor

## Torsocks

Instala **torsocks** en tu sistema

```
 	# apt install torsocks
```

Configura el fichero **/etc/tor/torrc** añadiendo al final de fichero las siguientes lineas:

```
 HiddenServiceDir /var/lib/tor/hidden_service/
 HiddenServicePort 80 127.0.0.1:8000
```

Y reinicia **torsocks**

```
 	# systemctl restart tor
```

## ¿Esto que hace? 

Por un lado, instalamos **torsocks** para conectarnos a la red de tor y con la configuración de HiddenService lo que estamos configurando es que cualquiera que accede a nuestra máquina a través de **Tor** por el puerto 80 (el puerto HTTP) podrá acceder a lo que nuestra máquina local (127.0.0.1) ofrezca en el puerto 8000, en nuestro caso, el docker que muestra Wordpress

## ¿Y que dirección tiene mi máquina en la red de **Tor?

Accede a _/var/lib/tor/hidden_service_ y encontraras un fichero llamado **hostname**, en ese fichero encontraras la dirección .onion a la que hay que acceder.

```
	 $ cat /var/lib/tor/hidden_service/hostname
	 e4spc5bweldxqdwiskjge3ei6c3flvjnafggl45rcuc7es7b6rw7afyd.onion
```

Si abrimos un Tor Browser y escribmos esta dirección, veremos nuestros Wordpress disponible a través de la red de Tor

Más información [https://www.torproject.org/docs/hidden-services.html.en]


# References

* https://buddy.works/guides/wordpress-docker-kubernetes-part-1


