---
title: Basic Terraform

---

# ¿ Que es Terraform ? 

Vamos a mirar de ir migrando nuestra infraestructura a Infrastructure as Code (IaC), es decir, abstraer nuestra infraestrutura para que a partir de recetas ejecutadas por aplicaciones llamadas CCA [Continuous configuration automation], como __Terraform__, podamos constuir, cambair o gestionar una infraestructura compleja de anera segura y replicarla con facilidad.

En vez de configurar las máquinas y servicios a través de la interfaz de usuario, generamos ficheros que ejecutan esos códigos por nosotor_s.

Nosotr_s no vamos a desplegar una infraestructura sobre los servicios de terceros, pero nos servirá para poder construir un esquema replicable a través de KVMs y Dockers en nuestra máquina local.

# Instalación

## Terraform CLI

 $ curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
 $ sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
 $ sudo apt-get update && sudo apt-get install terraform
terraform version

## Configurar Libvirt

Esto ya lo hemos visto en el apartado sobre _KVM_

## Terraform Libvrit Provider

Instalamos Golang, Git y Make
 
 $ sudo apt install golang git make

Ahora, compilasmo e instalamos el provider para libvirt

 $ mkdir -p $GOPATH/src/github.com/dmacvicar; cd $GOPATH/src/github.com/dmacvicar
 $ git clone https://github.com/dmacvicar/terraform-provider-libvirt.git
 $ cd $GOPATH/src/github.com/dmacvicar/terraform-provider-libvirt
 $ make install

Tedremos nuestro plugin en _$GOPATH/bin/terraform-provider-libvirt_

## Lo añadimos a los plugins de terraform

En las nuevas versiones de Terraform es necesario especificar correctament el provider para poder luego localizarlo en el fichero .tf. Por defecto, busca en el registry.terraform.io esos providers y como este provider de _libvirt_ no está disponible de manera oficial, debemos modificar ligeramente la ruta para que lo encuentre en nuestro máquina local.

Para ello, podemos estructurarlo como si de un _src_ de _GO_ y añadir la direccion del git junto con la version y la arquitectura. Así podemos gestionar mejor si tenemos diversas arquitecturas o versiones del provider.

Algo así:

 $ mkdir -p ~/.terraform.d/plugins/registry.terraform.io/dmacvicar/libvirt/0.6.2/linux_amd64  && cd $_

Copiamos el build generado anteriormente a esa carpeta:

 $ cp $GOPATH/bin/terraform-provider-libvirt ~/.terraform.d/plugins/registry.terraform.io/dmacvicar/libvirt/0.6.2/linux_amd64/

# Lanzamos nuestra infraestructura

Creamos una carpeta en nuestro _home_ para alojar el fichero de configuración y accedemos:

 $ mkdir terraform-libvirt & cd $_

Creamos un fichero como este llamado libvirt_simple.tf:

```
# definimos el origen del provider personalizado
terraform {
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.2"
    }
  }
}

# definimos el provider y el hypervisor que vamos a usar a traves de la URI
provider "libvirt" {
    uri = "qemu:///system"
}

# Definimos los recursos, una simple maquina virtual 
resource "libvirt_domain" "terraform_test" {
  name = "terraform_test"
}

```

Con esto, ya podemos validar

 $ terraform validate

Revisar que detecta el provider

 $ terraform providers

Inicializar

 $ terraform init

Planificar la modificación de la estructura

 $ terraform plan

Ejecutar el plan

 $ terraform apply

Y si observamos en nuestro hypervisor local, veremos el recurso creado

 $ virsh list

```
 Id   Name             State
--------------------------------
 4    terraform_test   running
```

Nuestra máquina no tiene sistema operativo, ni interfaz de red, .. 

Vamos a destruirla y crear una máquina con un sistema opertivo personalizado

 $ terraform destroy

Aceptamos y limpiamos así el recurso de nuestro sistema

# Cloud Init - Crear esquemas para máquinas virtuales

Antes de empezar, un problema generalizado es que cuando el provider descarga las imagenes, les aplica permisos restrictivos como _root_ y no nos permite realizar los cambios oportunos en la imagen a partir con cloud-init. La solución es desactivar la opción _security_driver_ de la configuración de _qemu_.

 $ vim /etc/libvirt/qemu.conf

```

# The default security driver is SELinux. If SELinux is disabled
# on the host, then the security driver will automatically disable
# itself. If you wish to disable QEMU SELinux security driver while
# leaving SELinux enabled for the host in general, then set this
# to 'none' instead. It's also possible to use more than one security
# driver at the same time, for this use a list of names separated by
# comma and delimited by square brackets. For example:
#
#       security_driver = [ "selinux", "apparmor" ]
#
# Notes: The DAC security driver is always enabled; as a result, the
# value of security_driver cannot contain "dac".  The value "none" is
# a special value; security_driver can be set to that value in
# isolation, but it cannot appear in a list of drivers.
#
#security_driver = "selinux"

security_driver = "none"


```

Ahora, deberemos definir nuestra plantilla _.tf_ de la manera siguiente:
...

# Links

 * https://learn.hashicorp.com/tutorials/terraform/infrastructure-as-code?in=terraform/aws-get-started
 * https://github.com/dmacvicar/terraform-provider-libvirt
 * https://fabianlee.org/2020/02/22/kvm-terraform-and-cloud-init-to-create-local-kvm-resources/
 * https://github.com/dmacvicar/terraform-provider-libvirt/issues/747#issuecomment-678575669
 * https://www.terraform.io/upgrade-guides/0-13.html
 * https://github.com/fkpwolf/terraform-provider-libvirt
 * https://medium.com/@yping88/building-and-installing-terraform-provider-for-libvirt-a08a29f93135
 * https://cloudinit.readthedocs.io/en/latest/topics/boot.html
 * https://github.com/dmacvicar/terraform-provider-libvirt/issues/546
 * https://en.wikipedia.org/wiki/Infrastructure_as_code
