---
title: Workshop Hugo
---

# Hugo Site Generator

https://gohugo.io

## Previo

* Revisa el manual de Markdown
* Revisa el manual de Docker y Docker Compose si quiere servir el site
* Revisa el manual de GIT

## Instalar HUGO

En Debian y similares podéis instarlo desde

	sudo apt install hugo

Aunque tenemos la opción de trabajar a través de Docker

Adjunto el docker-compose.yml en el repositorio. Requiere tener un proyecto ya generado con "hugo new site <sdsf>" así que podéis primero trabajar en casa y luego hacer un git push / git pull en el server para descargar el contenido y ejecutarlo via el docker-compose.

## Crear un site

Vamos a inicializar primero nuestro nuevo repositorio para luego poderlo compartir

	git init

Y ahora creamos el site con el comando **hugo**

	hugo new site workshop


Para poder ver algo, es importante descargarse un theme de la gran lista de [themes](https://themes.gohugo.io/) que hay disponible.

Elijo el [Hugo Cards](https://themes.gohugo.io/hugo-cards/) y copio el link de descarga y lo cargo como un submódulo en el proyecto:

	git submodule add https://github.com/bul-ikana/hugo-cards.git themes/hugo-cards

Con esto, tenemos un theme, en la misma página del theme donde podemos descargarlo (clonar el repository git), también nos documentan como funciona y el fichero de configuración (config.toml)  que se puede utilizar para este theme.

Este fichero lo podemos editar en la raiz de nuestro site.

Por defecto, tiene pocas cosas (url,codigo de lengua, título). Al instalar un tema añadimos nuevas variables para configurar el sitio (enlaces sociales, imágenes de cabecera, iconos, ...) depende de como sea el tema tendremos más o menos.

Edito el fichero config.toml, podéis revisarlo en el repositorio git del workshop.

Finalmente, podemos ejecutar el servidor de Hugo, para visualizar la página:

	hugo server -D

O a través del docker-compose en la carpeta raiz del proyecto:

	docker-compose up

Y podemos verificar que en la dirección http://localhost:1313 está disponible nuestro site

Ctrl+C para para el Docker y volvemos a la edición de contenidos.

Si lo arrancamos con docker-compose los ficheros generados para la visualización estan bajo su propiedad y no nos dejará usar **hugo server -D**, así que, decidir el formato más oportuno para vuestra instalación y trabajar con él durante el desarrollo del site.

## Edición de contenidos 

Para generar nuevas páginas en nuestro site

	hugo new posts/editando-con-hugo.md

Esto genera en la estructura de datos de **Hugo** el siguiente fichero:

	vim content/posts/editando-con-hugo.md

Ahora, solo debemos editar nuestro fichero Markdown con el contenido que queremos que tenga nuestro post.

En la cabecera, separada por "---" añadimos los metadatos o variables que tiene nuestro contenido.

Despues de eso, podemos escribir. 

Solo con la variable "title" deberíamos tener suficiente para poder publicar el post.

Este theme tiene por defecto dos páginas en el frontpage, son "pages/about" y "pages/contact".

Vamos a generarlas para poder poder editarlas

	hugo new pages/contact.md
	
	hugo new pages/about.md

Y las editamos a nuestro gusto.

## Generar el contenido estático

Cuando tengamos realizadas todas las pruebas con los contenidos, podemos buidear el site en estático.

Hasta ahora, hemos trabajado con el servidor de **Hugo** para hacer los previews, pero nos interesa generar los ficheros estáticos que son los que vamos a poder publicar en cualquier servidor HTML y luego poder replicarlo solo con copiarlos de un hosting a otro, sin PHP ni bases de datos.

Para ello, ejecutamos en la raiz del site:

	hugo -D

Y en la carpeta **public** tendremos los ficheros estáticos de nuestro site.

## Servir nuestro site

He dejado un docker-compose para usar el mismo server de **Hugo** o lo más coherente y ligero sería utilizar **LightHTTP**, un ligero servidor web que corre sobre casi todas las plataformas GNU/Linux y disponible para utilizar como imagen en Docker. 

Editamos el fichero docker-compose.lighttp.yml y lo guardamos como docker-compose.yml.

Luego, ejecutamos docker-compose:

	docker-compose up

Y cargamos en nuestro navegador en http://localhost:<puerto> con el puerto elegido.

# Gestión de contenidos en Hugo

La estructura de la sección de contenidos del site viene reflejada en la estructura de la carpeta contents.

Imagina que tenemos un site de este estilo

```
.
 content
  posts
   articulo1
   articulo2
  pages
   contact
   about
   more
    other0
    other1
```

Esto significa que la paǵina "other1", por ejemplo, será http://misitio/pages/more/other1

Para alterar este tipo de definición de las URLs, podemos usar en el archivo .md de la página las opciones:

 * url : indicando el path que queremos asignarle direcatmente. Por ejemplo: url: /blog/nueva_ruta
 * slug : indicando el nombre como queremos renombrar el site y que no sea el titulo. Por ejemplo: slug: "nuevo-nombre"
 * title: Es la única obligatoria para poder definir el path dentro del site y se usa si las otras dos no están disponible.

Este tipo de distribución, es muy sencilla pero limita un poco el potencial de **Hugo** 

Por tanto, vamos a empezar a trabajar el contenido a través de esta otra estructura:

```
.
 content
  posts
   articulo1
    index.md
   articulo2
    index.md
...
```
El resultado será el mismo, pero nos da la posibilidad a añadir otros recursos a cada uno de los artículos.

## Contenidos en las páginas

Creamos una página nueva con su index.md asociado:

	mkdir content/posts/completo
	vim content/posts/completo/index.md

Y aquí hacemos un post como los de antes.

Veréis que el resultado es el mismo que cuando lo hemos hecho sin el index.md

Copiamos una imagen en ese directorio, por ejemplo, image.jpg

Y usamos los Shortcodes de Hugo para añadirla al post:

{{< figure src="image.jpg" title="Mi fantástica imagen con pie" >}}
