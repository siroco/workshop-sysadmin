---
title: Sysadmin Workshop

---

Documentación generada durante las sesiones de discusión de Sysadmin

Recuerda que puedes convertir los ficheros _Markdown_ usando _Pandoc_

 $ pandoc --toc index.md -o FicheroPDF.pdf

O si lo prefieres ver por terminal puedes usar _grip_

 $ grip index.md

Y luego abrir la dirección que te indicará el mismo comando.

Por ejemplo, a través del terminal:

 $ w3m http://localhost:6419
