# Básicos con TMUX

## Instalación

	apt install tmux

## Fuera del terminal 

Nueva terminal
	
	tmux 

Nueva terminal con nombre de sesion

	tmux new -s hugo

Unirse al terminal

	tmux a

Unirse a un terminal concreto

	tmux a -t hugo

Listar sesiones

	tmux ls

Apagar sesion

	tmux kill-session -t hugo

La ejecución de comando se realiza a través de Ctrl+b

Es configurable a través de los ficheros de config tmux.conf en el $HOME

Revisar más documentación:

	man tmux

## Dentro del terminal

Antes de todos los comandos primero hay que pulsar Ctrl+b

 * Listar sesiones [s]
 * Nueva sesion [:new hugo2]
 * Nombre de la sesion [$]
 * Salir sin cerrar [d]

Ventanas

 * Nueva ventana [c]
 * Listar ventanas [w]
 * Cerrar ventana [&]

Paneles
 * Dividir en horizontal [%]
 * Dividir en vertial ["]
 * Cambiar de panel [o]
 * Cambiar el orden [O]
 * Cerrarlo [x]
 
Útiles

 * Reloj [t]
 * Shortcuts [?]
 * Linea de comandos [:]

## Otras referencias

 https://gist.github.com/henrik/1967800

## Visualizar un archivo markdown

	sudo apt install grip
 
	grip README.md

Abre en el navegador http://localhost:6419
