---
title: Vagrant

---

# About

> Vagrant is a tool for building and managing virtual machine environments in a single workflow. With an easy-to-use workflow and focus on automation, Vagrant lowers development environment setup time, increases production parity, and makes the "works on my machine" excuse a relic of the past.

La razón de hacer este ejercicio a través de Vagrant es para poder luego usar Float como sistema de gestión/orquestacion/... de nuestros servicios y a la vez, aislar los servicios en KVMs gestionados con Libvirt. 

Actualmetne, como entorno de desarrollo podrías usar sin problemas Docker, pero nuestra idea, es poder componer una infraestructura IaC con KVMs que alojan Dockers y poder replicarlo de una manera más o menos sencilla y segura.. 

Es cierto también, que el consumo de memoria por cada KVM y el consumo de espacio de disco, será mayor. Ganamos en aislamiento del contenido en esas máquinas siempre teniendo un buen firewall configurado en nuestro máqiuna madre para controlar culaquier interacción de las virtuales hacia la amfitriona.

Para esto, aquí hay cosas de Libvirt, Ansible, SSH, Git, Vagrant, Packer, Debian preseed, ... así que, revisa los otros manuales si algo de esto no sabes de que va.

# Install from Debian repository

Older version but checked by Debian community

 $ sudo apt install vagrant vagrant-libvirt

# Provider

> While Vagrant ships out of the box with support for VirtualBox, Hyper-V, and Docker, Vagrant has the ability to manage other types of machines as well. This is done by using other providers with Vagrant.

You can configure diferent providers for build vagrant projects

Provider are configured like _plugins_

Show plugins availables:

 $ vagrant plugin list

We will use _Libvirt_provider_

## Install libvirt provider
 
If you dont use repo packages, you need to install via source code libvirt-provider

From Hashicorp:

 $ sudo apt-get build-dep vagrant ruby-libvirt 

 $ sudo apt-get install qemu libvirt-daemon-system libvirt-clients ebtables dnsmasq-base

Install Vagrant provider like a plugin:

 $ vagrant plugin install vagrant-libvirt

# Boxes

> These base images are known as "boxes" in Vagrant, and specifying the box to use for your Vagrant environment is always the first step after creating a new Vagrantfile.

When you install a new Vagrant project, you download a box from scratch

I you want to download boxes before:

 $ vagrant box add generic/debian10

And then, when you create a new Vagrant project, edit Vagrantfile:

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/debian10"
end
```

Find more boxes [https://vagrantcloud.com/boxes/search]

# El entorno de trabajo

Vamos a suponer que confiamos en los _boxes_ publicados en la comunidad de Vagrant y usamos una imagen de Debian10 disponible para usar con nuestro provider de _libvirt_

 $ mkdir /tmp/vagrant_getting_started

 $ cd /tmp/vagrant_getting_started

 $ vagrant init generic/debian10

Se generará un fichero _Vagrantfile_ con la información mínima del proyecto.

El _box_ __generic/debian10__ es un box disponible para diversos providers (docker, libvirt, ...) pero antes, para poder usarlos, debemos instalar el plugin.

Si lo has instalado a través del paquete _vagrant-libvirt_ solo requieres ejecutar

 $ vagrant plugin install vagrant-libvirt

Y ahora si, ya puedes arrancar Vagrant:

 $ vagrant up

Y si todo es correcto

 $ vagrant ssh

Y tendrás acceso a tu nueva máquina virtual gestionada por Libvirt y Vagrant

By default, Vagrant shares your project directory (the one containing the Vagrantfile) to the /vagrant directory in your guest machine.


# Create your own Debian 10 box for libvirt

Install Packer >= 1.6.4

For Debian testing:

 $ apt install packer

Create a workdir directory

 $ mkdir ~/debian-custom

Write a template for Packer, like this:

```
{
  "variables":
    {
      "cpu": "2",
      "ram": "2048",
      "name": "debian",
      "disk_size": "40000",
      "version": "10",
      "iso_checksum_type": "sha256",
      "iso_urls": "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.6.0-amd64-netinst.iso",
      "iso_checksum": "2af8f43d4a7ab852151a7f630ba596572213e17d3579400b5648eba4cc974ed0",
      "headless": "true",
      "config_file": "debian-preseed.cfg",
      "ssh_username": "root",
      "ssh_password": "strong_passfrase",
    },
  "builders": [
    {
      "name": "{{user `name`}}{{user `version`}}",
      "type": "qemu",
      "format": "qcow2",
      "accelerator": "kvm",
      "qemu_binary": "/usr/bin/qemu-system-x86_64",
      "net_device": "virtio-net",
      "disk_interface": "virtio",
      "disk_cache": "none",
      "qemuargs": [[ "-m", "{{user `ram`}}M" ],[ "-smp", "{{user `cpu`}}" ]],
      "ssh_wait_timeout": "30m",
      "http_directory": ".",
      "http_port_min": 10082,
      "http_port_max": 10089,
      "host_port_min": 2222,
      "host_port_max": 2229,
      "ssh_username": "{{user `ssh_username`}}",
      "ssh_password": "{{user `ssh_password`}}",
      "iso_urls": "{{user `iso_urls`}}",
      "iso_checksum": "{{user `iso_checksum`}}",
      "boot_wait": "15s",
      "boot_command": [
                "<esc><wait>",
                "auto <wait>",
                "console-keymaps-at/keymap=es <wait>",
                "console-setup/ask_detect=false <wait>",
                "debconf/frontend=noninteractive <wait>",
                "debian-installer=es_ES <wait>",
                "fb=false <wait>",
                "install <wait>",
                "kbd-chooser/method=es <wait>",
                "keyboard-configuration/xkb-keymap=es <wait>",
                "locale=es_ES <wait>",
                "netcfg/get_hostname={{user `name`}}{{user `version`}} <wait>",
                "preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/http/{{user `config_file`}} <wait>",
                "<enter><wait>"
      ],
      "disk_size": "{{user `disk_size`}}",
      "disk_discard": "unmap",
      "disk_compression": true,
      "headless": "{{user `headless`}}",
      "shutdown_command": "echo '{{user `ssh_password`}}' | sudo -S shutdown -P now",
      "output_directory": "artifacts/qemu/{{user `name`}}{{user `version`}}"
    }
  ],
  "provisioners": [
    {
      "type": "shell",
      "execute_command": "{{.Vars}} bash '{{.Path}}'",
      "inline": [
        "echo \"deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main\" >> /etc/apt/sources.list",
        "apt -y install dirmngr",
        "apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367",
        "apt-get update",
        "apt -y install ansible"
        ]
    },
    {
      "type": "ansible-local",
      "playbook_file": "ansible/playbook.yml",
      "playbook_dir": "ansible"
    },
    {
      "type": "shell",
      "execute_command": "{{.Vars}} bash '{{.Path}}'",
      "inline": [
        "apt -y remove ansible",
        "apt-get clean",
        "apt-get -y autoremove --purge"
      ]
    }
  ],
  "post-processors": [{
    "type": "vagrant",
    "keep_input_artifact": true,
    "output": "debian10-custom.box"
  }]
}
```
Este fichero nos permite definir las variables con las que se construirá la maquina virtual en _libvirt_ y su instalación.

Primero configuramos las variables y las opciones de Packer para que peuda por ejemplo, en este caso, montar un server HTTP para la carga del _preseed_. Además, todo el proceso en el momento que la máquina está ya en instalación se podrá seguir a través de VNC con un cliente, como _vinagre_ sería algo así:

 $ vinagre vnc://127.0.0.1:5999

El puerto, 5999 en este caso, varia según la instalación, pero es informado durante el proceso de construcción de la imagen

Usamos un preseed para realizar la configuración de Debian automáticamente:

```
d-i pkgsel/install-language-support boolean false

# locale
d-i debian-installer/locale string en_ES.UTF-8

# keyboard
d-i keyboard-configuration/xkb-keymap   select  es

# timezone
d-i time/zone string Europe/Paris

# Controls whether to use NTP to set the clock during the install
d-i clock-setup/ntp boolean true

# apt
d-i mirror/country string manual
d-i mirror/http/hostname string ftp.debian.org
d-i mirror/http/directory string /debian
d-i mirror/http/proxy string
d-i apt-setup/use_mirror boolean true

# users
d-i passwd/root-password password testtest
d-i passwd/root-password-again password testtest
d-i passwd/make-user boolean false

# partitioning
d-i partman-auto/method string regular
d-i partman-auto/choose_recipe select atomic
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true

apt-cdrom-setup apt-setup/cdrom/set-first boolean false

# software
tasksel tasksel/first multiselect standard
d-i pkgsel/include string qemu-guest-agent wget openssh-server vim sudo cloud-init
popularity-contest popularity-contest/participate boolean false

# grub
d-i grub-installer/only_debian boolean true
d-i grub-installer/bootdev  string /dev/vda

# script
d-i preseed/late_command in-target sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /target/etc/ssh/sshd_config

# finish
d-i finish-install/reboot_in_progress note
```

Donde configuramos el idioma, hacemos una configuración estandard con el root con password _testest_, instalamos algunas utilididaes como _openssh-server_ y _sudo_, configuramos _grub_ y ofrecemos acceso SSH al usuario _root_ que por defecto viene desactivado sin clave SSH

Ahora, la sección _provisioners_ nos ofrece la posibilidad de ejecutar algunas rutinas para la _customización_ de esa imagen. En este caso, usamos una _playbook_ de __Ansible__ para hacer esa instalación usando una configuración basada en _roles_.

El main sería algo así:

```
---
- name: Configure KVM base image
  hosts: localhost
  gather_facts: True
  become: yes
  roles:
    - common

```

Configuración básica indicando que trabajaremos sobre localhost y usaremos el rol _common_

En la carpeta _roles/common_ tenemos las _tasks_ y las variables y ficheros necesarios para su ejecución:

El fichero _roles/common/tasks/main.yml_

```
---
- name: Configure GRUB to enable serial console on Debian/Ubuntu
  copy:
    src: grub.debian
    dest: /etc/default/grub
    mode: '0644'
    owner: root
    group: root
    backup: no
  when: ansible_distribution == 'Debian'

- name: Regenerate GRUB configuration
  command: "/usr/sbin/grub-mkconfig -o /boot/grub/grub.cfg"
  when: ansible_distribution == 'Debian'

- name: Tweak Debian network config
  copy:
    src: interfaces.debian
    dest: /etc/network/interfaces
    mode: '0644'
    owner: root
    group: root
    backup: no
  when: ansible_distribution == 'Debian'

- name: Add local pub key to authorized_keys
  authorized_key:
    user: vagrant
    key: "{{ lookup('file', 'id_rsa.pub') }}"
    state: present

- name: Tweak sshd_config
  lineinfile:
    dest: /etc/ssh/sshd_config
    regexp: ".*UseDNS.*"
    line: "UseDNS no"

- name: Install common packages
  package:
    name: "{{ item }}"
    state: latest
  loop: "{{ common_packages }}"

```

Lo básico es cambiar _grub_ para dar acceso por console a vagrant y añadir la clave ssh para acceder.

Para la clave, se usa un fichero __id_rsa__ que debemos generar en la sección files:

 $ ssh-keygen -q -t rsa -N '' -C 'packer-kvm-default-key' -f ./ansible/roles/common/files/id_rsa

__Importante__ Se deberá configurar la variable _config.ssh.private_key_path_ en el Vagrantfile con la dirección de la clave privada para arrancar la máquina virtual.

La historia del _UseDNS_ es para desactivar el uso de DNS si no tenemos configurado correctamente todavaía la infraestructura. Esto aligera el arranque de la máquina virtual.

El fichero _grub.debian_ ofrece el acceso por console y serial.

El fichero _interfaces.debian_ habilita dhcp en la interfaz _eth0_

Despues de eso, en _provisioners_ desintalamos Ansible y finalmente indicamos que se genere una box con el nombre _debian10-custom.box_ en la raiz del directorio donde estamos trabajando.

Para testear el fichero template de Packer, podemos usar:

 $ packer validate debian10.json

Y si es correcto, buildeamos con

 $ packer build debian10.json

El proceso de instalación lo podemos ir siguiente a través de VNC con el comando anteriormente comentado.

Al finalizar, si todo es correcto, tendremos un fichero _debian10.box_ que podremos incluir en nuestros _boxes_ de Vagrant usando:

 $ vagrant box add debian10.box --name mycustom/debian10

En el caso de querer rebuildear por algun error, en mi caso, no he conseguido hacerlo sin borrar la cache de Packer 

 $ rm -rf packer_cache

Aunque supongo que tiene que existir una método para reutilizar la cache y no volver a descargar la imagen desde cero. TODO: Buscar documentación

Para borrar un box anterior ya publicado en nuestro Vagrant:

 $ vagrant box remove mycustom/debian10

Y para usar nuestra imagen, creamos un nuevo directorio en nuestra carpeta local

 $ mkdir ~/vagrant_custom && cd $_
 
 $ vagrant init mycustom/debian10

Para acceder a nuestra imagen personalizada, necesitamos indicar a Vagrant que use nuestra SSH key generada y no la default de Vagrant. 

Copiamos la clave private a nuestro environment y la añadimos al fichero Vagrantfile generado al hacer el _init_.

```
...
  config.vm.box = "mycustom/debian10"
  config.ssh.private_key_path = "id_rsa"
...

```

También podríamos escribir ahí la ruta de donde se encuentra la clave privada con la que hemos trabajado creando la imagen.

Evitaremos así también que sea legible desde dentro de la máquina virtual.

 $ vagrant validate

 $ vagrant up

Y si todo ha ido bien, arrancará y podremos hacer un ssh:

 $ vagrant ssh

Y si está configurado  por defecto, tendremos NFS al directorio local donde hemos arrancado la imagen y podemos compartir archivos con el host.

Esto lo podéis ver tambien en la configuración del fichero /etc/exports que gestiona los directorios compartidos por NFS en el sistema.

Ahora, tenemos nuestra Debian base recién instalada para usarla como nos convenga y reusarla en todos las máquinas que queramos.

# Problemas 

Para _debugear_ el arranque de vagrant:

 $ VAGRANT_LOG=debug vagrant up

Aquí descubro cosas como que el firewall local bloquea el DHCP de las máquinas virtuales o que la clave SSH usada para la imagen no era la misma que usaba Vagrant para acceder.

# Write a provisioning script

You can configure bootstrap actions for this environament:

Create a bootstrap script named _bootstrap.sh_

```
#!/usr/bin/env bash

apt-get update
apt-get install -y apache2
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi
```
Install Apache and create a symlink to our vagrant sync folder

Like Docker but with a KVM and Vagrant-ized ;)

On Vagrantfile:

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/debian10"
  config.vm.provision :shell, path: "bootstrap.sh"
end
```

Update our config or Provisioning

 $ vagrant reload --provision

# Optional: Install Vagrant from Hashicorp

Si requieres una versión más actualizada que la de los repositorios de Debian, es necesario que descargues de la web de Hashicorp el paquete DEB.

Las nuevas versiones de Vagrant no funcionan correctamente con el Provider Libvirt de repositorios, así que, te tocará también compilar el plugin para usar como provider Libvirt y seguramente también te tocará compilar Packer para la nueva versión de Varnish.

Deberías realizar esto solo si es exclusivo una versión más actualizada que la que está disponible en los repositorios de Debian.

 $ sudo apt-get install libxslt-dev libxml2-dev libvirt-dev zlib1g-dev ruby-dev

 $ curl -O https://releases.hashicorp.com/vagrant/2.2.13/vagrant_2.2.13_x86_64.deb

 $ sudo apt update

 $ sudo apt install ./vagrant_2.2.13_x86_64.deb

 $ vagrant --version 


# Links

 * https://learn.hashicorp.com/tutorials/vagrant/getting-started-index?in=vagrant/getting-started
 * https://linuxize.com/post/how-to-install-vagrant-on-debian-10/
 * https://github.com/vagrant-libvirt/vagrant-libvirt
 * https://www.vagrantup.com/docs/boxes/base
 * https://github.com/fkolacek/packer-libvirt
 * https://www.packer.io/docs/builders/qemu.html
 * https://github.com/tylert/packer-build
 * https://github.com/goffinet/packer-kvm
 * https://www.packer.io/docs/templates/post-processors.html
 * https://stackoverflow.com/questions/31262833/how-to-create-a-vagrant-box-from-vmware-image-with-packer

# Next

 * __float: orquestation__ https://git.autistici.org/ai3/float/-/blob/master/docs/quickstart.md
