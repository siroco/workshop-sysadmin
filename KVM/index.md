---
title: Expand disk KVM

---

Resolución de dudas para expandir imágenes de disco en formato _qcow2_ 

# Cómo expandir un disco QCOW2 en nuestro KVM 

Primero de todo, deberemos apagar la máquina virtual y comprobar que realmente está apagada y no en modo reposo/pausa/... 

Llamaremos Server a la máquina que tiene KVM instalado y virtualiza.

Llamaremos Cliente a la máquina virtual dentro de Server.

Todo usando la distribución estable de GNU/Linux Debian.

## Preparamos la imagen para expandir

Apagamos la máquina virtual:

	sudo virsh shutdown <kvm_id>

Localizamos la imagen de disco, podemos hacerlo desde _virt-manager_

	sudo virsh vol-list default

Normalmente nuestra imagenes se encuentran en el _pool_ default y suele estar localizado en _/var/lib/libvirt/images_

O podemos investigar a través de un _dumpxml_ de nuestra máquina virtual:

	sudo virsh dumpxml <kvm_id>

Localizado, nos dirigimos a la carpeta oportuna y revisamos la información del volumen:

	sudo qemu-img info <image>.qcow2

Realiza una copia de seguirdad de esa imagen por si algo en el proceso no funciona:

 	sudo cp <image>.qcow2 <image>.backup.qcow2

o

	sudo dd if=<image>.qcow2 bs=4M of=<image>.backup.qcow2 status=progress

Ahora, indicamos cuanto queremos aportar a esa imagen:

	sudo qemu-img resize <image>.qcow2 +10G

Y volvemos a revisar que la imagen ha sido aumentada:

	sudo qemu-img info <image>.qcow2

Para evitar posibles problemas, haremos un check del disco antes de volver a levantar la virtual. Para ello a través del modulo _nbd_ podemos cargar en sistema imagenes qcow2

Tenemos dos opciones, expandir el disco en nuestro Cliente (guest) o hacerlo desde el servidor que hospeda.

## Expansión del disco desde el servidor

En el caso de hacerlo desde el servidor que hospeda.

Levantamos el módulo, si no lo está todavía:

	sudo modprobe nbd max_part=8
	
	sudo lsmod | grep nbd

Y conectamos la imagen al dispositivo nbd0, si no lo hemos utilizado para otra conexión:

	sudo qemu-nbd --connect /dev/nbd0 <image>.qcow2

Checkeamos el disco (ext4):
	
	sudo e2fsck -f /dev/nbd0p1

Expandimos el sistema de ficheros todo lo que hemos añadido al disco:

	sudo resize2fs /dev/nbd0p1

Se puede expandir una parte controlada, en nuestro caso, lo expandimos todo lo que se pueda.

Hacemos un nuevo check:
	
	sudo e2fsck -f /dev/nbd0p1

Y desconectamos la unidad

	sudo qemu-nbd --disconnect /dev/nbd0

Si nuestra unidad ha sido modificada en el proceso y ha perdido los permisos de acceso por nuestro usuario libvirt, realizamos el cambio:

	sudo chown libvirt-qemu:libvirt-qemu <image>.qcow2

## Expansión del disco desde la máquina virtual

Revisamos nuestro disco y la inforamción de espacio que tenemos disponible:

	sudo lsblk

Normalmetne, en el caso de maquinas virtuales, nuestro disco se llamará /dev/vdaX aunque en cada caso puede ser diferente.

Si no lo sabes, revisa tu _/etc/fstab_

Instalamos algunas utilidades:

	sudo apt -y install cloud-guest-utils gdisk

Y hacemos la expansión en caliente de nuestra partición 1 en /dev/vda

	sudo growpart /dev/vda 1

Si todo ha ido bien, indicará un estado de _CHANGED_ 

Si no ha funcionado, revisa el orden de tus particiones,
en nuestro caso, teníamos una partición expandida con la SWAP que no nos permitia extender la partición raiz.

La hemos borrado. descativado la swap en /etc/fstab  y hemos ejecutado otra vez el comando, y ha funcionado sin problemas. 

Ahora, hay que expandir el sistema de ficheros:

	sudo resize2fs /dev/vda1 

Si revisamos:

	sudo lsblk 

Y en el espacio de disco disponible:

	sudo df -h 

Podemos observar que ahora sí, nuestra partición tiene los G oportunos.


# Enlaces de interés

	* https://computingforgeeks.com/resize-ext-and-xfs-root-partition-without-lvm/
	* http://manpages.ubuntu.com/manpages/xenial/man1/growpart.1.html
	* https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/storage_administration_guide/ext4grow
