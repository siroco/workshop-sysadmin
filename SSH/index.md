---
title: SSH (Secure SHell)
---

# SSH JumpProxy

## Situation

Access to Computer B using as bastion Computer A via SSH

 * You are only access to Computer A using SSH
 * Computer B is accessible via Computer A using SSH

## Steps

Configure your .ssh/config

```
Host computerA
	HostName <IP or host adress Computer A>
	... (configure here other options to access Computer A)

Host computerB
	Hostname <IP o host address of Computer B via Computer A, like lan adress, ....>
	ProxyJump computerA # Use Computer A for access Computer B
	... (other options like IndentyFile, Port, User, ...)
```

Then

 $ ssh computerB

And welcome to Computer B usign Computer A as bastion

# Setup SSH socks tunnel

    $ ssh -N -D 9090 <server>

Maintain this shell opened and configure your Socksv5 Proxy on Firefox or Chrome

    * Manual Proxy
    * SocksHost : localhost
    * SocksPort : 9090
    * Proxy DNS when using Socks v5

# Setup redirect HTTP port over SSH

## Situation

Computer A

 * Has access to Internet
 * Has access to Computer B
 * SSH is installed

Computer B

 * Doesn't have access to Internet
 * OpenSSH Server is installed

## Steps

_ssh_ into Computer B from Computer A

 $ sudo ssh -R <selected port>:us.archive.ubuntu.com:80 user@computerb.host

Edit Computer B's /etc/apt/apt.conf to include the following lines:

 $ sudo vim /etc/apt/apt.conf

```
    Acquire::http::Proxy "http://localhost:<selected port>";
    Acquire::https::Proxy "https://localhost:<selected port>";
```

Run your _apt-get update_ or install or upgrade on Computer B and it should work.

## A few notes:

    You HAVE to keep the original session of ssh from Computer A to Computer B active while using Computer B to access apt-get repositories.
    You DON'T have to use the same ssh connection to utilize the tunnel (meaning if you have multiple ssh connection into Computer B, they should all work)

# SSH over TOR

## Install on GNU/Linux Debian derivated

 $ sudo apt install openssh-client

For network checks

 $ sudo apt install net-tools curl netcat

If you haven't installed yet

 $ sudo apt install tor torsocks

on others use **apk**, ....

## Check Tor Connections

Check if default __Tor Socks v5__ port is open:

 $ nc -zv 127.0.0.1 9050

For check your IP over tor:

 $ torsocks curl https://api.ipify.org

If not work, check **tor** is running 
	
 $ sudo systemctl status tor

If is _inactive_:

 $ sudo systemctl restart tor

And recheck status or system logs.

 $ sudo journalctl -xe

If is active, check your **torsocks** config:

 $ sudo gedit /etc/tor/torsocks.conf

_TorAddress_ and _TorPort_ may be the same in **torrc** config

 $ sudo gedit /etc/tor/torrc

If not, configure **torsocks** correctly

## Create a pair ssh keys

Generate our keys using ed25519 algothrim:

> In public-key cryptography, Edwards-curve Digital Signature Algorithm (EdDSA) is a digital signature scheme using a variant of Schnorr signature based on twisted Edwards curves.[1] It is designed to be faster than existing digital signature schemes without sacrificing security. It was developed by a team including Daniel J. Bernstein, Niels Duif, Tanja Lange, Peter Schwabe, and Bo-Yin Yang.[2] The reference implementation is public domain software.[3]
> https://en.wikipedia.org/wiki/EdDSA

On terminal:

 $ ssh-keygen -t ed25519 -f .ssh/id_sysadmin

## Config client

Example:

 $ cat .ssh/config

If not exists

 nano .ssh/config

and paste:

```ssh
Host *
	VisualHostKey yes
	Compression yes

Host *.onion
	ProxyCommand /bin/nc -xlocalhost:9050 -X5 %h %p

```

The First affect all our ssh connections.
 * **VisualHostKey** show fingerprint like a ascii image
 * **Compression** option that requests compression of all data being transferred over an SSH connection 


Second one affect our **ssh tor services** using onion domain
 * **ProxyCommand** avoid use **torsocks** everytime we use **ssh** command proxify all our connections via **netcat (nc)** connection. **Tor** is running a __Socksv5__ on localhost:9050. You can reconfigure this on your __torrc__ config file. Instead, you can use __.onion__ via torsocks: 

 $ torsocks ssh -l\<user\> -p\<port\> \<my_big_onion_v3_adress\>.onion

For our specific testbed:

```ssh
Host sysadmin
        Hostname <our_beatiful_big_long_onion_v3_address>.onion
        ProxyCommand /bin/nc -xlocalhost:9050 -X5 %h %p
        Port 22
        User root
        IdentityFile /home/<my_user_or_other_path>/.ssh/id_sysadmin

```

 * **Host** declare a custom hostname for ssh connection. Now we can use __sysadmin__ instead of large onion address.
 * **Hostname** original hostname aliased
 * **ProxyCoomand** why we use a custom hostname we need to reconfigure here our __proxycommand__ option
 * **Port** port ...
 * **User** user diferent from our local user
 * **IdentityFile**  where is our private ssh key pre-generated

More info:
 $ man ssh
 $ man ssh_config
 $ man nc
 $ man torsocks


## Configure Server

Copy your public key to your **.ssh/authorized_keys** user file.

If is _root_ : 

 $ nano /root/.ssh/authorized_keys

## Connect to server

 $ ssh sysadmin

## Copy the key to a server

Once an SSH key has been created, the ssh-copy-id command can be used to install it as an authorized key on the server. Once the key has been authorized for SSH, it grants access to the server without a password.

Use a command like the following to copy SSH key:

    ssh-copy-id -i ~/.ssh/mykey user@host

This logs into the server host, and copies keys to the server, and configures them to grant access by adding them to the authorized_keys file. The copying may ask for a password or other authentication for the server.

Only the public key is copied to the server. The private key should never be copied to another machine.

# Next steps (future)

 * Install **Monkeysphere** : GPG for SSH connections
