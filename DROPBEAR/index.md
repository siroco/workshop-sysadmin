---
title: Uso de Dropbear en initramfs para desencriptar volumenes LUKS en remoto

---

# Instalación

En entornos _Debian GNU/Linux_
 
En nuestra máquina remota:

 $ sudo apt instal dropbear dropbear-initramfs

Y en nuestra máquina local:

 $ ssh-keygen -t rsa -f ~/.ssh/id_diskluks

Usamos algoritmo RSA por la compatibilidad con dropbear, por lo menos, en la versión de Debian estable no nos permite usar algoritmos como ed25519 o similares.

# Copiamos nuestra clave pública

Como si de un acceso a una máquina remota por SSH, debemos configurar un fichero de claves autorizadas

En la máquina remota:

 $ sudo vim /etc/dropbear-initramfs/authorzied_keys

Y ahí pegamos nuestra clave pública, 
en nuestro caso podemos acceder a ella en nuestra máquina local:

 $ cat ~/.ssh/id_diskluks

Guardamos el fichero actualizado de authorized_keys y ahora configuramos los parámetros para el acceso cuando la máquina aún no ha arrancado

# Configurar red en el arranque de Dropbear

Como el sistema no está levantado, hay que configurar la red de nuestra interfaz para poder acceder a la consola de busybox en remoto

Editamos el fichero en la maquina remota:

 $ sudo vim /etc/initramfs-tools/initramfs.conf

Y bajo la linea _DEVICE=_ añadimos una linea nueva con la configuración de IP estática que será accesible desde la maquina local.

Imaginemos que tengo el servidor en una LAN con la dirección 192.168.1.0/24 y tenemos asociado un DCHP y otras cosas hasta la ip 192.168.1.200/24. 
En nuestro caso, vamos a asignarle la dirección 192.168.1.201/24 porque sabemos que no va estar ocupada.
Esto depende de cada red

Añadimos esta linea así:

 IP=192.168.1.201:255.255.255.0:192.168.1.1::enp6s0

Donde tenemos _IP:Netmask:Gateway::Interfaz_

Y guardamos el fichero.

Para que estos cambios queden realizado, hay que actualizar el sistema de arranca de nuestro sistema.

 $ sudo update-initramfs -u

Y nuestro Dropbear ya se iniciará al cargar el sistema justo antes de desencriptar los discos remotos

# Configurar SSH en la maquina local

Seguramente queramos tener almacenada la configuración para acceder a nuestro disco en la máquina local.

Editamos nuestro fichero local ~/.ssh/config

```
Host LuksRemote
	Hostname 192.168.1.201
	User root
	IdentityFile ~/.ssh/id_diskluks
	UserKnownHostsFile ~/.ssh/known_hosts.initramfs
```

Usamos la opción _UserKnownHostsFile_ para evitar colisionar con el fingerprint de otra maquina en la misma IP. Si por ejemplo, hemos configurado en la misma IP donde suele estar conectada esa máquina remota, tendremos un error de fingerprint porque tenemos almacenado en nuestro fichero de _knownhosts_ la anterior signatura digital. 

Ahora, reiniciamos el servidor y probamos

# Desencriptar disco Luks en Remoto

Si tenemos acceso al servidor, podemos mirar que en el arranque nos indica la IP de conexión y si hemos tenido algun fallo durante la configuración de Dropbear.

Si todo está bien,
desde nuestra máquina local podemos probar el acceso remoto:

 $ ssh LuksRemote

Y esto nos abre una sessión directamente en la consola de BusyBox

Aquí, para montar las unidades LUKS hay que usar el siguiente comando:

 $ cryptroot-unlock

Nos pedirá la passfrase de nuestra unidad LUKS y arrancará si todo eso correcto.

# Comando cryptroot-unlock en remoto

Para no tener que recordar el comando en BusyBox lo podemos añadir directamente en el fichero config de _dropbear_initramfs_

En la máquina remota:

 $ sudo vim /etc/dropbear-initramfs/config

En la variable __DROPBEAR_OPTIONS__ escribimos

 DROPBEAR_OPTIONS='-c cryptroot-unlock'

# Fuentes

 * https://wiki.debian.org/RescueInitramfs
 * https://dev.to/j6s/decrypting-boot-drives-remotely-using-dropbear-2hdf
