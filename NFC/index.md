---
title: NFC Workshop - Uso de tarjetas NFC bajo Gnu/Linux
---

# Sobre esta documentación

Esta documentación está escrita usando lenguaje **Markdown**

Puedes visualizar este archivo usando el comando **grip**

```
 $ grip <este_documento.md>
```

Y accediendo a la dirección indicada al ejecutar el programa.

O puedes convertirlo en un fichero PDF usando **pandoc**

```
 $ pandoc <este_document.md> -o <documento.pdf>
```

El ejercicio está desarrollado sobre una **Gnu/Linux Debian 10 64bits** pero debería ser funcional en cualquier otra distribución de **Gnu/Linux**.

Este documento no quiere profundizar en la securización de sistemas, pero en el desarrollo de las auditorias y análisis de seguridad de las tarjetas se usa software que podría ser malicioso para tu sistema.

El último capítulo de este documento recomienda algunos buenos usos en este tipo de trabajo.


# Herramientas básicas

Algunos comandos de utilidad:

Instalaremos **Git** para poder descargar algunos paquetes y los comandos básicos para compilar nuevos programas.

```
 $ sudo apt install git build-essential dh-autoreconf shtool vim
```

Y si no has instalado **Tor** todavía:

```
 $ sudo apt install torsocks
```

Además, para asegurar que los scripts usan **git** a través de **torsocks** podemos añadir el alias a nuestro fichero $HOME/.bashrc

```
 $ vim $HOME/.bashrc

## ADD

alias git='torsocks git'

## :x para guardar
```

# Uso de lectores NFC en Gnu/Linux

El uso de lectores con chipset ACR122U lo realizaremos a través de la librería **libnfc**

## Bloquear módulos en el kernel

Debemos prevenir la carga de algunos módulos para poder acceder a ellos a través de las herramientas de análisis.

Editamos **/etc/modprobe.d/blacklist-libnfc.conf**.

Añadimos las lineas:

```
blacklist pn533_usb
blacklist nfc
blacklist pn533
```

Revisamos que en nuestro kernel no estén cargados de antenamo:

```
 $ sudo modprobe -r pn533 pn533_usb nfc
```

## Instalación de libnfc y otras herramientas de análisis

Instalamos los paquetes:

```
 $ sudo apt install libnfc5 libnfc-bin mfoc mfuck
```

Probamos que **libnfc** está en uso:

```
 $ libnfc
```

Al no tener nada conectado, debería responder: **No NFC device found**

Con esto tendremos instalados los diversos comando para manipular los tags de nuestras tarjetas: nfc-emulate-forum-tag4, nfc-list, nfc-mfclassic, nfc-mfultralight, nfc-read-forum-tag3, nfc-relay-picc, nfc-scan-device.

## Conectar lector a la máquina virtual.

Para analizar posibles errores, lee el log de sistemas mientras realizas estos pasos:

```
 $ sudo tail -f /var/log/syslog
```

Para lxs que usan el lector en la maquina virtual, habrá que conectar el lector NFC y luego ir a **virt-manager** e indicar la conexión de este dipositivo USB en el menú de la gestión de nuestra máquina virtual "Redirigir dispositivo USB" y marcar la casilla de nuestro dispositivo USB lector.

Si miramos el log, veremos que se ha registrado un nuevo dispositivo USB en nuestra máquina.

Si leemos el siguiente mensaje, es posible que no se haya aplicado nuestro blacklist de los módulos

```
 usbcore: registered new interface driver pn533_usb
```

Revisa la configuración del blacklist, reinicia la máquina virtual y repite estos mismos pasos.

## nfc-list

Probamos ahora:

```
 $ nfc-list -v
```

Y esto nos indicará que la interfaz está conectada.

Probamos de poner sobre el dispositivo una tarjeta NFC (llamado en terminos técnicos NFC TAG) y volvemos a relanzar el comando.

Si la tarjeta es compatible con nuestro lector, veremos la información de sus cabeceras: Tipo de tarjeta ISO, UID, ATQA, SAK.

Estos datos son básicos para enteder el tipo de tarjeta con el que estamos trabajando y valorar las posibles herramientas de análisis.

La documentación sobre los tipos de tarjetas NFC ISO14443A la puedes localizar aquí:

http://www.nfc-tools.org/index.php?title=ISO14443A

En el caso de las tarjetas ISO14443B puedes encontrar un poco de información aquí:

http://www.nfc-tools.org/index.php?title=ISO14443B

# Recuperar claves de los bloques en tarjetas Mifare Classic

Vamos a enfocarnos en los casos más simples como las **tarjeta Mifare Classic 1K** (ISO14443A con ATQA 00 04 SAK 08 con UID de 4 bytes) ya que tenemos a disposición mucha documentación y diversos programas dedicados a este tipo de tarjeta.

Este tipo de tarjetas no deberían tardar en desaparecer ya que sus fallos de seguridad están publicados desde antes de 2007 y aunque hay algunas versiones con mejoras, suelen ser fácilmente explotables.

Por lo general, la tarjetas NFC Mifare contienen 16 bloques duales (a/b) para almacenar la información.

En el caso de las tarjeta más comunes, las Mifare 1K el contenido almacenado en ellas es de 1K. Pero existen diversos tipos, cada vez con sistemas más avanzados de almacenamiento y seguridad de sus contenidos.

Para poder leer estos 32 bloques, es necesaria una clave comunmente tratada en hexadecimal de 6 bytes. Si no se dispone de la clave, no es posible acceder a su contenido.

Un ejemplo: 0xffffffffffff

Si nuestra intención es hacer una clonación para analizar esas tarjetas, deberemos conseguir las 32 claves que desbloquearan los 32 bloques.

Una opción sin tener que instalar parte de todo este software es usar la distribución [Kali Linux](https://www.kali.org/), preparada para realizar auditorias y otros análisis de seguridad.


## Técnica Nesting attack usando MFOC

https://github.com/nfc-tools/mfoc

Los sistemas más vulnerables, utilizan la misma clave para todos los bloques, y muchas veces utilizan las claves más estandarizadas para realizar esta labor. En estos casos, podemos usar una herramienta muy útil llamada **mfoc** que además de hacer un análisis de la tarjeta, indicarnos su tipología, testea las claves más utilizadas, hace un test de vulnerabilidades de claves llamado _"nesting"_ y si consigue descifrar los bloques, crea una imagen de nuestra tarjeta en un fichero binario.

Probamos nuestra tarjeta con **mfoc** almacenando su resultado en un fichero binario:

```
 $ mfoc -O mycard.mfoc
```

Analicemos el resultado, veremos el tipo de tarjeta Mifare y otros datos de interés de la construcción de la tarjeta, además, veremos la realización de las pruebas de acceso a los bloques y si es posible, realizará el __dump__ de su contenido al fichero __mycard.mfoc__

Si la tarjeta no es Mifare Classic, **mfoc** indicará que no es posible leerla.

**mfoc: ERROR: only Mifare Classic is supported**

Si la tarjeta no tiene un NFC o no es compatible con MFOC, el resultado será algo así:

**mfoc: ERROR: No tag found.**

En estos casos, usa **nfc-list** para identificar la tarjeta

## Técnica Darkside - Uso del comando mfcuk

Esta técnica es utilizada cuando no tenemos a disposición ninguna clave. Es la técnica más costoso a nivel de tiempo y no tiene porque ofrecer resultados.

Por lo general, para testear si la tarjeta es vulnerable a este ataque, se suele atacar un bloque específico y valorar si el resultado es asequible a nivel de computación en nuestra máquina.

```
 $ mfcuk -h
```

Probamos de explotar el bloque 0A :

```
 $ mfcuk -C -R 0:A -s 250 -S 250 -v 3
```

Y revisamos el log. **mfcuk** irá testeando el bloque con su algoritmo intentando descrifrarlo. Si lo consigue, nos indicará la clave de ese bloque.

Con esa clave, podemos ejecutar **mfoc** con la opción _-k <clave_recuperada>_ para testear si es compatible con otros bloques.

## HardNested Attack - miLazyCracker


En un paper de Carlo Meijer y Roel Verdult se describe un nuevo ataque denominado "Nested attack". Este necesita como mínimo de conocer una clave de un bloque A o B. El ataque parece consistir en hacer diversas tentativas de autentificaciones que permiten recolectar "nonces cifradas únicas" y permiten por esta razón reducir las posibles claves para hacer un ataque de fuerza bruta.

La herramienta desarrollada para este tipo de ataques se llama **miLazyCracker** y data de finales de 2017. Es necesario usar una versión parcheada de **mfoc** aunque tiene unos scripts de autoconfiguración.

Este tipo de ataque requiere tener como mínimo una clave de algun bloque. A través de esa clave se van recuperando otras claves de otros bloques.

Empezamos descargando el código de **mfoc**:

```
 $ torsocks git clone https://github.com/ilumitr/miLazyCracker
 $ cd miLazyCracker
 $ ./miLazyCrackerFreshInstall.sh
```

Si el script ha funcionado, podemos usar el comando:

```
 $ libnfc_crypto1_crack <clave_conocida> \
      <bloque_clave_conocida> <A|B> \
      <bloque_a_crackear> <A|B>
```

Algo de terminología para entender las opciones:
* __Clave Conocida:__ Es la clave que sabemos ya sea porque la hemos desencriptado con **mfcuk** o porque es una clave default o porque la hemos desencriptado anteriormente.
* __Bloque de la clave conocida:__ Este es el bloque que hace referencia a la clave que usamos como primaria. La identificación del bloque en este caso funciona diferente a **mfoc**. Aquí los bloques se calculan como múltiples de 4, es decir, el bloque 0 del módulo A es 0 A, el bloque 1 del módulo A es 4 A el bloque 8 del módulo A es 32 A. Para los B es lo mismo, el bloque 8 del módulo B es 32 B.
* __Bloque a crackear:__ Es el bloque que queremos probar de crackear.

Si el resultado es correcto, verás un mensaje **Found key: <clave>**

Ahora, puedes volver a **mfoc** y probar esa clave a través de la opción _-k_

Si necesitas más claves, deberás repetir este paso las bloques necesarios.

### Errores en la compilación de myLazyCracker

Revisa los errores del script y realiza la compilación manualmente, primero compilando y parcheando **mfoc** y luego compilando **crypto1_bs**. Las recetas están escritas en los directorios de ambos códigos, en el fichero _Makefile_, y en los scripts de instalación como _miLazyCrackerFreshInstall.sh_.

En la carpeta de **miLazyCracker** puedes descargar el código de **mfoc**, aplicarle los parches y compilarlo.

```
 $ sudo apt install libnfc-dev pkg-config
 $ torsocks git clone https://github.com/nfc-tools/mfoc.git
 $ cd mfoc
 $ patch -p1 < ../mfoc_test_prng.diff
 $ patch -p1 < ../mfoc_fix_4k_and_mini.diff
 $ patch -p1 < ../mfoc_support_tnp.diff
 $ patch -p1 < ../mfoc_support_2k.diff
 $ autoreconf -is # Aquí revisamos posibles errores
 $ ./configure
 $ make
```

Los parches para **mfoc** están pensados una versión antigua a la 0.10.7 o mayores, así que es lógico que tengas errores porque los parches han modificado lineas que no son las correctas. Deberías revisar esos parches, modificarlos para que apliquen los cambios a las lineas oportunas y si no lo consigues, probar de compilar sin aplicarlos y ver si el programa funciona.

A este documento se adjuntan también las librerías para compilar **crypto1_bs**: craptev1-v1.1.tar.xz y crapto1-v3.3.tar.xz

Estas librerías parecen no estar disponibles ya para su descarga en el repositorio público y hay que buscarlas en otras webs no tan públicas como un repositorio, por lo tanto, su uso en este caso no es seguro pudiendo contener algún _código malicioso_. **No recomendamos su instalación en máquinas no controladas.**

Si realizas la compilación manualmente y no realizas un _make install_ puedes ejecutar el comando **libnfc_crypto1_crack** entrandolo en la carpeta **crypto1_bs**:

```
 $ cd crypto1_bs
 $ ./libnfc_crypto1_crack <clave_conocida> \
      <bloque_clave_conocida> <A|B> \
      <bloque_a_crackear> <A|B>
```


# Lectura y edición de ficheros binarios

Con **mfoc** tendremos una copia de nuestra tarjeta. Ahora, podemos intentar leer su contenido, modificarlo, ...

Para ello, deberemos pasar a un lenguaje editable el fichero generado por **mfoc**

Por ejemplo, podemos instalar el paquete de **hexdump** y su uso es bastante simple:

```
 $ hexdump -vC dump.mfd
```

Esto nos devolverá la información de ese documento en formato hexadecimal legible.

Otra opción es usar **xxd**

```
 $ xxd dump.mfd
```

Y para guardar el resultado en un fichero de texto:

```
 $ xxd dump.mfd > dump.txt
```

Y a la inversa:

```
 $ xxd -r dump.txt > dump.mfd
```

Tener las imágenes en formato textual nos permite poder compararlas y buscar diferencias usando el comando **diff**

```
 $ diff imagen001.mfd imagen002.mfd
```

Y con esto, encontrar diferencias al realizar diversas acciones en la tarjeta.

Para convertir un valor de hexadecimal a decimal. Por ejemplo el valor "0xff":

```
 $ echo $((16#ff))
 255
```
Otra opción, usar **bc**
```
 $ sudo apt install bc
 $ echo "obase=10; ibase=16; FF" | bc
```

Si queremos trabajar con ASCII, lo más cómodo es trabajar con **xxd**
```
 $ echo Hello World | xxd
 00000000: 4865 6c6c 6f20 576f 726c 640a            Hello World.
```

Y esto lo podremos adaptar a nuestra imagen y reescribir datos.


# Copiar datos a la tarjeta Mifare Classic

Si hemos conseguido leer los bloques, tendremos un fichero binario generado con **mfoc** con los datos de esa tarjeta. A ese fichero le llamaremos **original.mfd**.

**CUIDADO**, manipular el contenido de las tarjetas puede dejarlas inservibles. Utilizar tarjetas de testeo. Recordad que una gran mayoría de tarjetas tienen bloqueada la reescritura del Sector 0, donde se almacen el UID y otros datos de la tarjeta, por lo tanto, no sería posible clonar el UID de otra tarjeta, ni modificarlo.

Para escribir una imagen sobre una tarjeta, primero deberemos tener todas sus claves en un fichero, que llamaremos **keys.txt** y construir una imagen actual, que llamaremos **now.mfd**.

Situamos la tarjeta sobre el lector y ejecutamos el siguiente comando:

```
 $ mfoc -k keys.txt -O now.mfd
```

Ahora, en **now.mfd** tenemos la tarjeta actual junto con sus claves.

Lo siguiente es sobre-escribir esa tarjeta con la imagen **original.mfd**

Para ello se debe ejecutar la reescritura en los dos módulos A/B de toda la tarjeta de la siguiente manera con la tarjeta sobre el lector:

```
 $ nfc-mfclassic w a now.mfd original.mfd
 $ nfc-mfclassic w b now.mfd original.mfd
```

Si es correcto el proceso, nos indicara los datos escritos sobre la tarjeta.

Para más información:

```
 $ nfc-mfclassic --help
```

# Creación de un espacio seguro de desarrollo e investigación

Lo más seguro sería instalar una distribución [Qubes OS](https://www.qubes-os.org/) y generar máquinas virtuales para cada una de las tareas a realizar. **Recomendamos altamente esa manera de trabajar.**

En el caso de solo disponer de una máquina con una distribución Gnu/Linux Debian o derivadas, deberemos construir nuestras propias máquinas virtuales para este trabajo y securizar correctamente nuestra conexión a internet y el acceso de estas máquinas virtuales a la red. Recuerda torificar tu tráfico y aísla la máquinas a través de algún cortafuegos.

## QEMU/KVM

Sigue la Wiki de Debian para instalar el sistema de virtualización Qemu/KVM

https://wiki.debian.org/KVM


## Virsh

Configuramos el acceso al grupo de virtualización para poder trabajar con nuestra usuaria local con **libvirt**

```
 $ sudo adduser <mi_usuaria> libvirt
```

Salimos de la sesión actual de usuario y volvemos a acceder para que el sistema nos identifique el cambio de grupo.

Testeamos el cambio de grupos

```
 $ groups
```

Si estamos en el grupo **libvirt**, podemos hacer los siguiente:

```
 $ virsh list --all
```

Algunos comandos útiles con vish:

```
 $ virsh start <nombremaquina> # arrancamos la máquina

 $ virsh shutdown <nombremaquina> # paramos la maquina

 $ virsh autostart <nombremaquina> # autoarranque al iniciar sistema

 $ virsh autostart <nombremaquina> --disable # descactivar autoarranque

 $ virsh console <nombremaquina>  # accesso via terminal si está activado en guest

 $ virsh net-list # muestra las interfaces virtuales creadas

 $ virsh pool-list # muestra los "grupos de volumenes" disponibles

 $ virsh vol-list default # muestra las imagenes descargadas

 $ virsh vol-list KVMdisks # muestra los volumenes asociados a nuestras máquinas
```

Igualmente, si entramos sin asignar un comando, virsh nos ofrece una consola para ejecutar estos comandos

```
 $ virsh
```

Y si tenemos dudas

```
 $ vish help
```

## Virt-Manager

Para agilizar el trabajo con la máquina virtual usaremos **virt-manager**

```
 $ sudo apt install virt-manager
```

Iniciamos **virt-manager**

```
 $ virt-manager
```

## Instalación máquina invitada

Ahora, lo que hacemos es crear una nueva máquina virtual.

Si queremos una máquina nueva y limpia, descargamos una imagen en Debian 10 en nuestro host:

```
 $ wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.3.0-amd64-netinst.iso
 $ sudo mv debian*.iso /var/lib/libvirt/images/
```

Click-amos sobre nuestra conexión local  y luego:

 Archivo > Nueva maquina Virtual

* Conexión : QEMU/KVM
* Medio de instalación local (usaremos una imagen de debian descargada en /var/lib/libvirt/images, si queréis otras imágenes, se pueden descargar o subir allí mismo)
* Elegimos la imagin .iso de Debian 10 AMD disponible
* Memoria: 512MB
* CPU: 1
* Almacenamiento: Creamos un disco del tamaño oportuno para nuestra máquina (4G x ejemplos)
* Último paso: revisamos el config

Para iniciar, doble click sobre ella y al botón de "Play" para arrancar y hacemos la instalación como en cualquier otra Debian.

En el caso de usar una maquina virtual ya creada, debería arrancarnos sin problemas.

Si no es así, revisar en el config la asignación del disco QCOW2 para ese máquina y que el arranque de sistema se realice contra ese disco.

## Post-Instalación

### Torificar la instalación a través de apt

Para torificar el tráfico de instalación de paquetes, usaremos el transport de tor disponible en los repositorios:

```
 $ sudo apt install apt-transport-tor tor
```

Y modificamos en nuestro _/etc/apt/sources.list_ las entradas a través del siguiente patrón usado en **vim**:

```
 $ sudo vim /etc/apt/sources.list

:%s/http:/tor+http:/g
:x
```
Ahora hacemos actualizamos repositorios y revisamos que use **socksv5** para la descarga:

```
 $ sudo apt update
```

### Acceso via SSH

Por defecto el SSH está desactivado y el usuario **root** no tenía passwd.

```
 $ sudo apt install openssh-server
 $ sudo vim /etc/ssh/sshd_config
```

Y añadimos/modificamos la entrada:

```
PermitRootLogin yes # solo en el caso de usar esto como máquina local
```

Lo más seguro es no activar esta opción y añadir nuestras claves **ssh** a **/root/.ssh/authorized_keys**.

Y para activar el SSH tuvimos que hacer:

```
 $ sudo dpkg-reconfigure openssh-server
 $ sudo systemctl daemon-reload
 $ sudo systemctl restart ssh
```

### Activar el acceso por consola a la máquina virtual generada (guest)

Es posible que no nos interesa tener acceso SSH a esa máquina, así que, existe la opción de acceder a través de consola.

Debemos acceder a esa como usuaria "root" máquina a través de virt-manager.

1. Activamos en systemctl el acceso _serial console_:

```
 $ sudo systemctl enable serial-getty@ttyS0.service
 $ sudo systemctl start serial-getty@ttyS0.service
```

2. Salimos de la máquina virtual, y ahora desde la máquina madre, podemos acceder a la consola a través del comando **"console"*** de virsh

```
 $ sudo virsh console <mi_maquina>
```

# Enlaces
* https://oneguyoneblog.com/2015/09/16/acr122u-nfc-usb-reader-raspberry-pi/
* https://medium.com/@decrocksam/cracking-mifare-classic-nfc-cards-using-the-hardnested-attack-506aab3ea305
* http://www.nfc-tools.org/index.php/MIFARE_Classic
* https://xorhak.io/manage-d8-selecta-credit-on-your-rfid-card/
* https://p-te.fr/2016/06/08/tuto-recharger-carte-nfc/
* http://arishitz.net/coffee-nfc-exploit-coffee-again/
* https://dyrk.org/2015/09/03/faille-nfc-distributeur-selecta/
* https://firefart.at/post/how-to-crack-mifare-classic-cards/
* http://www.cs.ru.nl/~flaviog/publications/Attack.MIFARE.pdf
* http://www.cs.ru.nl/~flaviog/publications/Dismantling.Mifare.pdf
* http://www.cs.ru.nl/~flaviog/publications/Pickpocketing.Mifare.pdf
* http://www.fuzzysecurity.com/tutorials/rfid/2.html
* http://cache.nxp.com/documents/data_sheet/MF1S50YYX_V1.pdf
