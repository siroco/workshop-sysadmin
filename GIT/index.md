---
title: GIT Basics
---

# Comandos básicos

Iniciar un repositorio:

	git init

Añadir un archivo al indice:

	git add <archivo>

Commit de los ADDs,RMs o cambios de los ficheros indexados:

	git commit -am "Comentario de los cambios"

Eliminar un fichero del indice y del árbol de archivos

	git rm <archivo>

Eliminar solo del indice

	git rm --cached <archivo>

Pushear contenido al servidor a la rama MASTER
	
	git push -u origin master

Descargar novedades del repo

	git pull

Clonar un repositorio 

	git clone <repositorio.git>

Añadir un submodulo de otro repositorio

	git submodule add <repositorio.git> path/destino/submodulo
