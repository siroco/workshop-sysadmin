---
title: Dockerize Tor Service
---

# Pre

 * Install Docker
 * Run Docker over non-root user without sudo

# Edit config

Edit **torrc.default** using **vim** or **nano**:

	$ vim torrc.default

Change basic config:

```sh
SocksPort 0.0.0.0:9100 # Bind to this address:port too.

# Hidden Service - Redirecto to service IP on Docker Network
# HiddenServiceDir /var/lib/tor/hidden_service/
# HiddenServicePort 80 127.0.0.1:80

```

# Check Dockerfile

```sh
FROM alpine:latest
RUN apk update && apk add \
    tor \
    --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ \
    && rm -rf /var/cache/apk/*
EXPOSE 9050
COPY torrc.default /etc/tor/torrc.default
RUN chown -R tor /etc/tor
USER tor
VOLUME /var/lib/tor
ENTRYPOINT [ "tor" ]
CMD [ "-f", "/etc/tor/torrc.default" ]
```

# Build image

	$ docker build -t piperrak/torproxy

# Run image

	$ docker run -d --restart always -p 9051:9100 --name torproxy piperrak/torproxy

# Check image is running

	$ docker ps

or

	$ docker ps --all

# Check image is proxing

Your public IP:

	$ curl -L http://ifconfig.me

Your torified IP:

	$ curl --socks5 http://localhost:9051 -L http://ifconfig.me

# Linkography

 * https://sachsenhofer.io/setup-tor-docker-container/ 
 * https://phoenixnap.com/kb/docker-volumes
